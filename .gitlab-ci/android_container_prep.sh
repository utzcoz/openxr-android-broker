#!/bin/bash
# Copyright 2018-2023, Collabora, Ltd. and the Monado contributors
# SPDX-License-Identifier: BSL-1.0

(
    set -e
    cd "$(dirname "$0")"
    bash ./install-ndk.sh
    bash ./install-android-sdk.sh
)
